import { connect } from 'react-redux';
import React from 'react';
import LogicGate from './logicGate';
import * as WireStates from '../wireStates';

class Constant extends React.Component {
  static inputs() {
    return [];
  }

  static output() {
    return {x: 65, y: 25};
  }

  static calculateOutput(inputs, block) {
    return block.state;
  }

  static toggleState(state) {
    return state === WireStates.ON ? WireStates.OFF : WireStates.ON;
  }

  render() {
    return (
      <LogicGate id="constant" name="CONST" {...this.props}>
        <path d="M 50,15 L 30,15 L 30,35 L 50,35 L 50,13" />
        <path
          d="M 50,25 L 65,25"
          stroke={WireStates.stateToColor(this.props.state)}
          connection={{x: 65, y: 25}}
          />
        <text
          fill="black"
          strokeWidth="1"
          fontSize="14"
          x="40"
          y="30"
          textAnchor="middle"
          style={{userSelect: 'none'}}
          >
          {WireStates.stateToText(this.props.state)}
        </text>
      </LogicGate>
    );
  }
}

Constant.defaultProps = {
  x: 0,
  y: 0
};

function mapStateToProps(state, ownProps) {
  return {
    state: state.blocks[ownProps.blockId].state
  };
}

export default connect(mapStateToProps)(Constant);
