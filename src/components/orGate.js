import { connect } from 'react-redux';
import { posEq } from '../utils';
import * as WireStates from '../wireStates';
import React from 'react';
import LogicGate from './logicGate';

class OrGate extends React.Component {
  static inputs() {
    return [
      {x: 5, y: 15},
      {x: 5, y: 35},
    ];
  }

  static output() {
    return {x: 75, y: 25};
  }

  static calculateOutput(inputs) {
    if (inputs.length === 0) {
      return WireStates.UNDEFINED;
    }
    return inputs.some(input => input === WireStates.ON) ? WireStates.ON : WireStates.OFF;
  }

  render() {
    return (
      <LogicGate id="or_gate" name="OR" {...this.props}>
        <path d="M 5,15 L 20,15"
              connection={{x: 5, y: 15}}
              stroke={this.props.inputColors[0]}
              />
        <path d="M 5,35 L 20,35"
              connection={{x: 5, y: 35}}
              stroke={this.props.inputColors[1]}
              />
        <path d="M 13,4.5 A 30,30 0 0 1 13,44.5 L 30.1,44.5 C 32.5,44.5 37.8,44.5 44.8,42.0 C 49.7,39.6 56.3,34.7 61.4,25 C 51.1,4.7 34.9,4.5 30.1,4.5 L 13,4.5 Z" />
        <path
          d="M 60.4,25 L 75,25"
          stroke={this.props.outputColor}
          connection={{x: 75, y: 25}}
          />
      </LogicGate>
    );
  }
}

OrGate.defaultProps = {
  x: 0,
  y: 0
};

function mapStateToProps(state, ownProps) {
  const block = state.blocks[ownProps.blockId];
  const inputColors = {};
  block.connections && block.connections.forEach(connection => {
    inputColors[OrGate.inputs().findIndex(pos => posEq(pos, connection.pos))] =
      WireStates.stateToColor(state.workspace.wires[connection.wireId].state);
  });
  return {
    outputColor: WireStates.stateToColor(block.state),
    inputColors
  };
}

export default connect(mapStateToProps)(OrGate);
