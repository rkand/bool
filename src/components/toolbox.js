import { connect } from 'react-redux';
import React from 'react';

class Toolbox extends React.Component {
  render() {
    return (
      <g>
        <rect id="toolbar_background" x="0" y="0" width="250" height="100%" fill="#F34440" />
        <text id="toolbar_header" textAnchor="middle" x="125" y="50" fill="white" fontSize="25" style={{userSelect: 'none'}} >
          Toolbox
        </text>
        <g id="toolbar_components">
          {this.props.children}
        </g>
      </g>
    );
  }
}

function mapStateToProps(state) {
  return {
    children: state.toolbox.blocksById.map(blockId => {
      const block = state.blocks[blockId];
      return React.createElement(block.type, {
        x: block.pos.x,
        y: block.pos.y,
        blockId: blockId,
        key: blockId
      });
    })
  };
}

export default connect(mapStateToProps)(Toolbox);
