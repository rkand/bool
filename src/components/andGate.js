import React from 'react';
import LogicGate from './logicGate';
import * as WireStates from '../wireStates';
import { connect } from 'react-redux';
import { posEq } from '../utils';

class AndGate extends React.Component {
  static inputs() {
    return [
      {x: 5, y: 15},
      {x: 5, y: 35},
    ];
  }

  static output() {
    return {x: 75, y: 25};
  }

  static calculateOutput(inputs) {
    if (inputs.length === 0) {
      return WireStates.UNDEFINED;
    }
    return inputs.every(input => input === WireStates.ON) ? WireStates.ON : WireStates.OFF;
  }

  render() {
    return (
      <LogicGate id="and_gate" name="AND" {...this.props}>
        <path d="M 5,15 L 20,15"
              connection={{x: 5, y: 15}}
              stroke={this.props.inputColors[0]}
              />
        <path d="M 5,35 L 20,35"
              connection={{x: 5, y: 35}}
              stroke={this.props.inputColors[1]}
              />
        <path d="M 20,5 L 20,45 L 40,45 C 51.7,45 60,35 60,25 C 60,14.0 51.7,5 40,5 L 20,5 Z" />
        <path
          d="M 60.4,25 L 75,25"
          stroke={this.props.outputColor}
          connection={{x: 75, y: 25}}
          />
      </LogicGate>
    );
  }
}

AndGate.defaultProps = {
  x: 0,
  y: 0
};

function mapStateToProps(state, ownProps) {
  const block = state.blocks[ownProps.blockId];
  const inputColors = {};
  block.connections && block.connections.forEach(connection => {
    inputColors[AndGate.inputs().findIndex(pos => posEq(pos, connection.pos))] =
      WireStates.stateToColor(state.workspace.wires[connection.wireId].state);
  });
  return {
    outputColor: WireStates.stateToColor(block.state),
    inputColors
  };
}

export default connect(mapStateToProps)(AndGate);
