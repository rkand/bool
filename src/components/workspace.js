import { connect } from 'react-redux';
import React from 'react';
import Wire from './wire';

class Workspace extends React.Component {

  _onMouseDown(event) {
    console.log('workspace mousedown');
  }

  render() {
    return (
      <g >
        <defs>
          <pattern id="dot_grid" width="10" height="10" patternUnits="userSpaceOnUse">
            <rect width="10" height="10" fill="#51B2AF" />
            <circle cx="5" cy="5" r="1.5" fill="#ACCF6A" />
          </pattern>
        </defs>

        <rect
          onMouseDown={this.props.onMouseDown}
          id="workspace_background"
          x="0"
          y="0"
          width="100%"
          height="100%"
          fill="url(#dot_grid)"
          />

        <g id="workspace_components">
          {this.props.children}
        </g>
      </g>
    );
  }
}

function mapStateToProps(state) {
  return {
    children: state.workspace.blocksById.map(blockId => {
      const block = state.blocks[blockId];
      return React.createElement(block.type, {
        x: block.pos.x,
        y: block.pos.y,
        blockId: blockId,
        key: blockId
      });
    }).concat(Object.keys(state.workspace.wires).map((wireId) => {
      return (
        <Wire
          coords={state.workspace.wires[wireId].coords}
          key={`wire_${wireId}`}
          wireIndex={wireId}
          />
      );
    }))
  };
}

export default connect(mapStateToProps)(Workspace);
