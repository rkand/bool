import { connect } from 'react-redux';
import { roundPos } from '../utils';
import React from 'react';
import Wire from './wire';

class DraggedBlock extends React.Component {
  render() {
    return (
      <g>
        {this.props.children}
      </g>
    );
  }
}

function mapStateToProps(state) {
  // TODO(ram): Change drag states to constants after refactoring reducers.js
  switch (state.dragState.dragging) {
  case 'dragging_block':
    return blockDragProps(state);
  case 'dragging_wire':
  case 'extending_wire':
    return wireDragProps(state);
  default:
    return {};
  }
}

function blockDragProps(state) {
  const block = state.blocks[state.dragState.blockId];

  return {
    children: [
      React.createElement(block.type, {
        x: block.pos.x + state.dragState.mousePos.x - state.dragState.mouseStart.x,
        y: block.pos.y + state.dragState.mousePos.y - state.dragState.mouseStart.y,
        blockId: state.dragState.blockId,
        key: state.dragState.blockId
      })
    ]
  };
}

function wireDragProps(state) {
  const roundedMousePos = roundPos(state.dragState.mousePos);
  const coords = [[
    state.dragState.mouseStart,
    {
      x: roundedMousePos.x,
      y: state.dragState.mouseStart.y
    },
    roundedMousePos,
  ]];
  return {
    children: [(
      <Wire coords={coords} key="dragging_wire" wireIndex={-1} />
    )]
  };
}

export default connect(mapStateToProps)(DraggedBlock);
