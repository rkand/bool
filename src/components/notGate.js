import { connect } from 'react-redux';
import * as WireStates from '../wireStates';
import React from 'react';
import LogicGate from './logicGate';

class NotGate extends React.Component {
  static inputs() {
    return [
      {x: 5, y: 25},
    ];
  }

  static output() {
    return {x: 75, y: 25};
  }

  static calculateOutput(inputs) {
    if (inputs.length !== 1) {
      return WireStates.UNDEFINED;
    }
    switch (inputs[0]) {
    case WireStates.ON:
      return WireStates.OFF;
    case WireStates.OFF:
      return WireStates.ON;
    default:
      return inputs[0];
    }
  }

  render() {
    return (
      <LogicGate id="not_gate" name="NOT" {...this.props}>
        <path d="M 5,25 L 20,25"
              connection={{x: 5, y: 25}}
              stroke={this.props.inputColor}
              />
        <path d="M 20,5 L 20,45 L 50,25 Z" />
        <circle cx="55" cy="25" r="5" />
        <path
          d="M 60,25 L 75,25"
          stroke={this.props.outputColor}
          connection={{x: 75, y: 25}}
          />
      </LogicGate>
    );
  }
}

NotGate.defaultProps = {
  x: 0,
  y: 0
};

function mapStateToProps(state, ownProps) {
  const block = state.blocks[ownProps.blockId];
  const inputColor = block.connections && block.connections[0] &&
        WireStates.stateToColor(state.workspace.wires[block.connections[0].wireId].state);
  return {
    outputColor: WireStates.stateToColor(block.state),
    inputColor
  };
}

export default connect(mapStateToProps)(NotGate);
