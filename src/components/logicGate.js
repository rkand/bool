import { connect } from 'react-redux';
import React from 'react';
import * as Actions from '../actions';

class LogicGate extends React.Component {

  constructor(props) {
    super(props);
    this.blockId = props.blockId;
    this.state = {
      showText: false
    };
  }

  _onMouseOut(event) {
    this.setState({
      showText: false
    });
  }

  _onMouseOver(event) {
    this.setState({
      showText: true
    });
  }

  render() {
    let helperText = null;
    if(this.state.showText) {
      helperText =
        <text
      id={'helper_text_' + this.props.id}
      x="40"
      fill="white"
      fontSize="14"
      textAnchor="middle"
        >
        {this.props.name}
      </text>;
    }
    const childrenWithEventHandlers = React.Children.map(
      this.props.children,
      child => {
        const { connection, ...props} = child.props;
        props.onMouseDown = connection ?
          event => this.props.startWire(event, this.blockId, connection) :
          event => this.props.mouseDownGate(event, this.blockId);
        return <child.type {...props} />;
      });
    return (
      <g
        onMouseOut={this._onMouseOut.bind(this)}
        onMouseOver={this._onMouseOver.bind(this)}
        transform={`translate(${this.props.x}, ${this.props.y})`}>
        <g id={this.props.id} stroke="black" strokeWidth="4" fill="#F7BA63">
          {childrenWithEventHandlers}
        </g>
        {helperText}
      </g>
    );
  }
}

LogicGate.defaultProps = {
  x: 0,
  y: 0
};

const mapDispatchToProps = (dispatch) => {
  return {
    mouseDownGate: (event, blockId) => {
      if (event.buttons & 1) {
        dispatch({
          type: Actions.START_DRAG,
          mousePos: {
            x: event.clientX,
            y: event.clientY
          },
          blockId
        });
      } else if (event.button & 2) {
        dispatch({
          type: Actions.TOGGLE_GATE,
          blockId
        });
      }
      event.preventDefault(); // Needed for Firefox to allow dragging correctly
    },
    startWire: (event, blockId, connection) => {
      dispatch({
        type: Actions.START_WIRE,
        connectionPos: connection,
        mousePos: {
          x: event.clientX,
          y: event.clientY
        },
        blockId
      });
      event.preventDefault();
    }
  };
};

export default connect(null, mapDispatchToProps)(LogicGate);
