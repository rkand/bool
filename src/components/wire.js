import { connect } from 'react-redux';
import { EXTEND_WIRE, DELETE_WIRE } from '../actions';
import { stateToColor, UNDEFINED } from '../wireStates';
import React from 'react';

class Wire extends React.Component {

  render() {
    const coords = this.props.coords;
    if (!coords) {
      return null;
    }
    const branches = coords.map((branch, index) => {
      const path = 'M ' + branch.map(coord => `${coord.x},${coord.y}`).join(' L ');
      return <path d={path} key={index} onMouseDown={event => this.props.onMouseDown(event, +this.props.wireIndex)}/>;
    });
    return (
      <g stroke={stateToColor(this.props.state)} strokeWidth="4" strokeLinecap="round" fill="none">
        {branches}
      </g>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    onMouseDown: (event, wireIndex) => {
      if (event.button === 2) { // Right mouse-button
        dispatch({
          type: DELETE_WIRE,
          wireIndex: wireIndex
        });
      }
      if (event.buttons & 1) {
        dispatch({
          type: EXTEND_WIRE,
          mousePos: {
            x: event.clientX,
            y: event.clientY
          },
          wireIndex
        });
      }
    }
  };
}

function mapStateToProps(state, ownProps) {
  const wire = state.workspace.wires[+ownProps.wireIndex];
  return {
    state: wire ? wire.state : UNDEFINED
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Wire);
