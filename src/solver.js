import { posEq } from './utils';
import * as WireState from './wireStates';

export default function updateWireStates(state) {
  let updated = true;
  let iterations = 0;
  const newWorkspaceState = Object.assign({}, state.workspace);
  const newBlockState = Object.assign({}, state.blocks);
  while (updated) {
    iterations += 1;
    if (iterations === 100) {
      console.warn('Solver is taking a while. Bad feedback or just really complex?');
    } else if (iterations > 200) {
      throw new Error('Solver took too long');
    }
    updated = false;
    newWorkspaceState.blocksById.forEach(blockId => {
      const block = newBlockState[blockId];
      if (block.needsUpdate) {
        const inputs = block.connections.filter(connection => {
          return block.type.inputs().find(input => posEq(input, connection.pos));
        }).map(connection => {
          return newWorkspaceState.wires[connection.wireId].state;
        });
        const output = block.type.calculateOutput(inputs, block);
        block.state = output;
        const outputConnection = block.connections.find(
          connection => posEq(block.type.output(), connection.pos));
        if (outputConnection) {
          const outputWire = newWorkspaceState.wires[outputConnection.wireId];
          if (outputWire.state !== output) {
            outputWire.needsUpdate = true;
            updated = true;
          }
        }
        block.needsUpdate = false;
      }
    });
    Object.keys(newWorkspaceState.wires).forEach(wireId => {
      let wire = newWorkspaceState.wires[wireId];
      if (wire.needsUpdate) {
        wire = Object.assign({}, wire, { needsUpdate: false });
        newWorkspaceState.wires[wireId] = wire;
        const outputConnections = wire.connections.filter(connection => {
          return posEq(
            newBlockState[connection.blockId].type.output(),
            connection.pos);
        });
        if (outputConnections.length === 0) {
          if (wire.state === WireState.UNDEFINED) {
            return;
          }
          wire.state = WireState.UNDEFINED;
        } else {
          let state = newBlockState[outputConnections[0].blockId].state;
          if (outputConnections.some(connection => {
            return newBlockState[connection.blockId].state !== state;
          })) {
            state = WireState.ERROR;
          }
          if (state === wire.state) {
            return;
          }
          wire.state = state;
        }
        wire.connections.filter(connection => {
          return newBlockState[connection.blockId].type.inputs().find(input => {
            return posEq(input, connection.pos);
          });
        }).forEach(connection => {
          newBlockState[connection.blockId] = Object.assign({},
                                                            newBlockState[connection.blockId], { needsUpdate: true });
          updated = true;
        });
      }
    });
  }
  if (iterations > 1) {
    return Object.assign({}, state, {
      workspace: newWorkspaceState,
      blocks: newBlockState
    });
  }
  return state;
}
