export const START_WIRE = 'start_wire';
export const EXTEND_WIRE = 'extend_wire';
export const DELETE_WIRE = 'delete_wire';

export const START_DRAG = 'start_drag';
export const END_DRAG = 'end_drag';

export const MOUSE_MOVE = 'mouse_move';

export const ADD_TO_TOOLBOX = 'add_to_toolbox';
export const TOGGLE_GATE = 'toggle_gate';
