import { blocks } from './reducers/blocksReducer';
import { dragState } from './reducers/dragStateReducer';
import { toolbox } from './reducers/toolboxReducer';
import { workspace } from './reducers/workspaceReducer';

import updateWireStates from './solver';

export const NOT_DRAGGING = 'not_dragging';
export const DRAGGING_BLOCK = 'dragging_block';
export const DRAGGING_WIRE = 'dragging_wire';
export const EXTENDING_WIRE = 'extending_wire';

export const initialState = {
  dragState: {
    dragging: NOT_DRAGGING
  },
  toolbox: {
    blocksById: []
  },
  workspace: {
    blocksById: [],
    wires: {}
  },
  blocks: {
  },
};

export function reduce(state = initialState, action) {
  var newState = state;
  newState = toolbox(newState, action);
  newState = workspace(newState, action);
  newState = dragState(newState, action);
  newState = blocks(newState, action);
  if (newState !== state) {
    newState = updateWireStates(newState);
  }
  return newState;
}
