export function roundPos(pos, precision = 10, offset = 5) {
  return {
    x: Math.round((pos.x - offset) / precision) * precision + offset,
    y: Math.round((pos.y - offset) / precision) * precision + offset
  };
}

export function addPos() {
  return Array.prototype.reduce.call(arguments, addTwo);
}

function addTwo(a, b) {
  return {
    x: a.x + b.x,
    y: a.y + b.y
  };
}

export function multPos(s, a) {
  return {
    x: a.x * s,
    y: a.y * s
  };
}

export function posEq(a, b) {
  return a.x === b.x && a.y === b.y;
}

export function getNextId(object) {
  return Math.max(...Object.keys(object), -1) + 1;
}

export function createNewBlockState(state, newBlockId, block) {
  const newBlockState = Object.assign({}, state.blocks);
  newBlockState[newBlockId] = block;
  return newBlockState;
}
