import { END_DRAG, START_DRAG, DELETE_WIRE } from '../actions';
import { NOT_DRAGGING, DRAGGING_BLOCK, DRAGGING_WIRE, EXTENDING_WIRE } from '../reducers';
import { roundPos, addPos, multPos, posEq, getNextId } from '../utils';
import { OFF } from '../wireStates';

export function workspace(state, action) {
  switch (action.type) {
  case END_DRAG:
    return endDrag(state, action);
  case START_DRAG:
    return startDrag(state, action);
  case DELETE_WIRE:
    return deleteWire(state, action);
  default:
    return state;
  }
}

function startDrag(state, action) {
  const blockIndex = state.workspace.blocksById.indexOf(action.blockId);
  if (blockIndex !== -1) {
    const blocksById = state.workspace.blocksById;
    return Object.assign({}, state, {
      workspace: Object.assign({}, state.workspace , {
        blocksById: blocksById.slice(0, blockIndex).concat(blocksById.slice(blockIndex + 1))
      })
    });
  } else {
    return state;
  }
}

function deleteWire(state, action) {
  let newWorkspaceState, newBlockState;
  newWorkspaceState = Object.assign({}, state.workspace);
  newWorkspaceState.wires = Object.assign({}, state.workspace.wires);
  const wire = newWorkspaceState.wires[action.wireIndex];
  newBlockState = Object.assign({}, state.blocks);
  wire.connections.forEach(connection => {
    const block = Object.assign({}, newBlockState[connection.blockId]);
    block.connections = block.connections.filter(
      blockConnection => blockConnection.wireId !== action.wireIndex);
    newBlockState[connection.blockId] = block;
  });
  delete newWorkspaceState.wires[action.wireIndex];
  return Object.assign({}, state, {
    workspace: newWorkspaceState,
    blocks: newBlockState
  });
}

function endDrag(state, action) {
  let newWorkspaceState, newBlockState, endPos, connections;
  const dragState = state.dragState;
  switch (dragState.dragging) {
  case NOT_DRAGGING:
    return state;
  case DRAGGING_BLOCK:
    const pos = roundPos(addPos(
      dragState.blockStartPos,
      dragState.mousePos,
      multPos(-1, dragState.mouseStart)), 10, 0);
    if (pos.x < 200) {
      return state;
    }
    const newBlock = Object.assign({}, state.blocks[dragState.blockId], {
      pos,
      needsUpdate: true
    });
    newBlockState = Object.assign({}, state.blocks);
    newBlockState[dragState.blockId] = newBlock;

    newWorkspaceState = Object.assign({}, state.workspace, {
      blocksById: state.workspace.blocksById.concat([dragState.blockId])
    });

    return Object.assign({}, state, {
      blocks: newBlockState,
      workspace: newWorkspaceState
    });
  case DRAGGING_WIRE:
    endPos = roundPos(state.dragState.mousePos);
    const coords = [[
      state.dragState.mouseStart,
      {
        x: endPos.x,
        y: state.dragState.mouseStart.y
      },
      endPos,
    ]];
    const newWireId = getNextId(state.workspace.wires);
    const newWiresState = Object.assign({}, state.workspace.wires);
    newWiresState[newWireId] = {
      coords,
      state: OFF,
      connections: [],
      needsUpdate: true
    };
    newWorkspaceState = Object.assign({}, state.workspace, {
      wires: newWiresState
    });

    connections = [{
      blockId: state.dragState.connectionBlockId,
      pos: state.dragState.connectionPos
    }];
    state.workspace.blocksById.find(blockId => {
      const block = state.blocks[blockId];
      const connectionPos = block.type.inputs().concat([block.type.output()]).find(input => {
        return posEq(addPos(block.pos, input), endPos);
      });
      if (connectionPos) {
        connections.push({
          blockId,
          pos: connectionPos
        });
        // find() stops looping once we return true
        return true;
      }
      return false;
    });
    newWorkspaceState.wires[newWireId].connections.push(...connections);

    newBlockState = Object.assign({}, state.blocks);
    connections.forEach(connection => {
      newBlockState[connection.blockId].needsUpdate = true;
      newBlockState[connection.blockId].connections.push({
        wireId: newWireId,
        pos: connection.pos
      });
    });

    return Object.assign({}, state, {
      blocks: newBlockState,
      workspace: newWorkspaceState
    });
  case EXTENDING_WIRE:
    let wire = Object.assign({}, state.workspace.wires[state.dragState.wireIndex]);
    endPos = roundPos(state.dragState.mousePos);
    wire.coords = wire.coords.concat([[
      state.dragState.mouseStart,
      {
        x: endPos.x,
        y: state.dragState.mouseStart.y
      },
      endPos,
    ]]);
    connections = wire.connections.slice();

    state.workspace.blocksById.find(blockId => {
      const block = Object.assign({}, state.blocks[blockId]);
      const connectionPos = block.type.inputs().concat([block.type.output()]).find(input => {
        return posEq(addPos(block.pos, input), endPos);
      });
      newBlockState = state.blocks;
      if (connectionPos) {
        connections.push({
          blockId,
          pos: connectionPos
        });

        newBlockState = Object.assign({}, newBlockState);
        newBlockState[blockId] = block;
        block.needsUpdate = true;
        block.connections = block.connections.concat({
          wireId: state.dragState.wireIndex,
          pos: connectionPos
        });

        // find() stops looping once we return true
        return true;
      }
      return false;
    });
    wire.connections = connections;

    newWorkspaceState = Object.assign({}, state.workspace);
    newWorkspaceState.wires = Object.assign({}, newWorkspaceState.wires);
    newWorkspaceState.wires[state.dragState.wireIndex] = wire;

    return Object.assign({}, state, {
      workspace: newWorkspaceState,
      blocks: newBlockState
    });
  default:
    return state;
  }
}
