import { START_DRAG, START_WIRE, EXTEND_WIRE, END_DRAG, MOUSE_MOVE } from '../actions';
import { NOT_DRAGGING, DRAGGING_BLOCK, DRAGGING_WIRE, EXTENDING_WIRE } from '../reducers';
import { roundPos, addPos, getNextId, createNewBlockState } from '../utils';

export function dragState(state, action) {
  switch (action.type) {
  case START_DRAG:
    return startDrag(state, action);
  case START_WIRE:
    return startWire(state, action);
  case EXTEND_WIRE:
    return extendWire(state, action);
  case END_DRAG:
    return endDrag(state, action);
  case MOUSE_MOVE:
    return mouseMove(state, action);
  default:
    return state;
  }
};

function startDrag(state, action) {
  let newDragState = state.dragState;
  let newBlockState = state.blocks;
  let blockId = action.blockId;
  let block = state.blocks[blockId];
  if (block.copyOnDrag) {
    blockId = getNextId(state.blocks);
    newBlockState = createNewBlockState(
      state,
      blockId,
      Object.assign({}, block, {
        copyOnDrag: false,
        connections: []
      })
    );
  }
  newDragState = {
    dragging: DRAGGING_BLOCK,
    mouseStart: action.mousePos,
    mousePos: action.mousePos,
    blockStartPos: block.pos,
    blockId: blockId
  };
  if (newDragState !== state.dragState || newBlockState !== state.blocks) {
    return Object.assign({}, state, {
      dragState: newDragState,
      blocks: newBlockState
    });
  } else {
    return state;
  }
}

function startWire(state, action) {
  let newDragState = state.dragState;
  let newBlockState = state.blocks;
  newDragState = {
    dragging: DRAGGING_WIRE,
    mouseStart: addPos(state.blocks[action.blockId].pos, action.connectionPos),
    mousePos: action.mousePos,
    connectionPos: action.connectionPos,
    connectionBlockId: action.blockId
  };
  if (newDragState !== state.dragState || newBlockState !== state.blocks) {
    return Object.assign({}, state, {
      dragState: newDragState,
      blocks: newBlockState
    });
  } else {
    return state;
  }
}

function extendWire(state, action) {
  let newDragState = state.dragState;
  let newBlockState = state.blocks;
  newDragState = {
    dragging: EXTENDING_WIRE,
    mouseStart: roundPos(action.mousePos),
    mousePos: action.mousePos,
    wireIndex: action.wireIndex
  };
  if (newDragState !== state.dragState || newBlockState !== state.blocks) {
    return Object.assign({}, state, {
      dragState: newDragState,
      blocks: newBlockState
    });
  } else {
    return state;
  }
}

function endDrag(state, action) {
  let newDragState = state.dragState;
  let newBlockState = state.blocks;
  newDragState = {
    dragging: NOT_DRAGGING
  };
  if (newDragState !== state.dragState || newBlockState !== state.blocks) {
    return Object.assign({}, state, {
      dragState: newDragState,
      blocks: newBlockState
    });
  } else {
    return state;
  }
  
}

function mouseMove(state, action) {
  let newDragState = state.dragState;
  let newBlockState = state.blocks;
  if (newDragState.dragging !== NOT_DRAGGING) {
    newDragState = Object.assign({}, state.dragState, {
      mousePos: action.mousePos
    });
  }
  if (newDragState !== state.dragState || newBlockState !== state.blocks) {
    return Object.assign({}, state, {
      dragState: newDragState,
      blocks: newBlockState
    });
  } else {
    return state;
  }
}



