import { ADD_TO_TOOLBOX } from '../actions';
import { getNextId, createNewBlockState } from '../utils';

export function toolbox(state, action) {
  switch (action.type) {
  case ADD_TO_TOOLBOX:
    return addToToolbox(state, action);
  default:
    return state;
  }
}

function addToToolbox(state, action) {
  const nextId = getNextId(state.blocks);
  const nextYPos = 80 + state.toolbox.blocksById.length * 80;
  const newBlocks = createNewBlockState(state, nextId, {
		type: action.block,
		copyOnDrag: true,
		pos: {
			x: 85,
			y: nextYPos
		},
		...action.blockProps
	});
	const newToolbox = Object.assign({}, state.toolbox, {
		blocksById: state.toolbox.blocksById.concat([nextId])
	});
	return Object.assign({}, state, {
		toolbox: newToolbox,
		blocks: newBlocks
	});
}
