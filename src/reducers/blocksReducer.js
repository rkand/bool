import { TOGGLE_GATE } from '../actions';

export function blocks(state, action) {
  switch (action.type) {
  case TOGGLE_GATE:
    return toggleGate(state, action);
  default:
    return state;
  }
}

function toggleGate(state, action) {
  const newBlockState = Object.assign({}, state.blocks);
  const block = Object.assign({}, newBlockState[action.blockId]);
  newBlockState[action.blockId] = block;
  if (block.type.toggleState) {
    block.state = block.type.toggleState(block.state);
    block.needsUpdate = true;
    return Object.assign({}, state, {
      blocks: newBlockState
    });
  } else {
    return state;
  }
}
