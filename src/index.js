import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore } from 'redux';

import { reduce, initialState } from './reducers';
import DraggedBlock from './components/draggedBlock';
import Toolbox from './components/toolbox';
import Workspace from './components/workspace';

import * as Actions from './actions';
import * as WireStates from './wireStates';
import AndGate from './components/andGate';
import OrGate from './components/orGate';
import NotGate from './components/notGate';
import Constant from './components/constant';

const store = createStore(reduce, initialState);
window.store = store;

export default class App extends React.Component {
  render() {
    return (
      <svg width='100%' height='100%'>
        <g fontFamily="Helvetica" fontWeight="bold" >
          <Workspace />
          <Toolbox />
          <DraggedBlock />
        </g>
      </svg>
    );
  }
}

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
);

[AndGate, OrGate, NotGate].forEach(gate => store.dispatch({
  type: Actions.ADD_TO_TOOLBOX,
  block: gate,
  blockProps: {}
}));

[WireStates.ON, WireStates.OFF].forEach(state => store.dispatch({
  type: Actions.ADD_TO_TOOLBOX,
  block: Constant,
  blockProps: {
    state
  }
}));

document.addEventListener('mousemove', event => {
  store.dispatch({
    type: Actions.MOUSE_MOVE,
    mousePos: {
      x: event.clientX,
      y: event.clientY
    },
  });
});

document.addEventListener('mouseup', event => {
  store.dispatch({
    type: Actions.END_DRAG
  });
});

// Suppresses right click menu
window.oncontextmenu = function() {
  return false;
};
