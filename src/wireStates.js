export const ON = 'wire_on';
export const OFF = 'wire_off';
export const UNDEFINED = 'wire_undefined';
export const ERROR = 'wire_error';

export function stateToText(state) {
  switch(state) {
  case ON:
    return '1';
  case OFF:
    return '0';
  default:
    return '0';
  }
}

export function stateToColor(state) {
  switch(state) {
  case ON:
    return 'green';
  case OFF:
    return 'black';
  case ERROR:
    return 'red';
  default:
    return 'grey';
  }
}
